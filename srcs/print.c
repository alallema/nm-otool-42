/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 17:28:19 by alallema          #+#    #+#             */
/*   Updated: 2019/01/29 16:22:18 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void		putnbr_base(unsigned long n, int base)
{
	char const	*char_base = "0123456789abcdef";

	if (n > ((unsigned long)base - 1))
		putnbr_base(n / base, base);
	write(1, &(char_base[n % base]), 1);
}

void		print_arg(char *arg)
{
	ft_putstr("\n");
	ft_putstr(arg);
	ft_putstr(":\n");
}

int			char_to_hexa(void *file, uint8_t c)
{
	(void)file;
	if (c < 16)
		ft_putchar('0');
	putnbr_base(c, 16);
	return (0);
}

void		print_value(uint64_t value, void *file, char c)
{
	char	*str;
	char	*str_value;
	size_t	len;

	len = 0;
	if (((t_header *)file)->arch == ARCH_32)
		len = 9;
	if (((t_header *)file)->arch == ARCH_64)
		len = 17;
	str = ft_memalloc(len);
	str_value = ft_itoa_base(value, 16);
	if (value || c != 'U')
	{
		if (len > ft_strlen(str_value))
			ft_memset(str, '0', len - ft_strlen(str_value) - 1);
		else
			ft_memset(str, '0', len - ft_strlen(str_value));
	}
	else
		ft_memset(str, ' ', len - 1);
	ft_putstr(str);
	ft_putstr(str_value);
	ft_memdel((void *)&str);
	ft_memdel((void *)&str_value);
}

void		print_nlist(void *list, void *file)
{
	t_nlist	*elem;
	char	c;

	elem = list;
	while (elem)
	{
		if ((N_TYPE & endian(elem->type, file)) == N_UNDF)
			c = elem->value ? 'c' : 'u';
		else if ((N_TYPE & endian(elem->type, file)) == N_PBUD)
			c = 'u';
		else if ((N_TYPE & endian(elem->type, file)) == N_SECT)
			c = get_sect_type(((t_header *)file)->sect, elem->nsect);
		else if ((N_TYPE & endian(elem->type, file)) == N_ABS)
			c = 'a';
		else if ((N_TYPE & endian(elem->type, file)) == N_INDR)
			c = 'i';
		if (N_EXT & endian(elem->type, file) && c != '?')
			c -= 32;
		print_value(elem->value, file, c);
		ft_putstr(" ");
		ft_putchar(c);
		ft_putstr(" ");
		ft_putendl(elem->strx);
		elem = elem->next;
	}
}
