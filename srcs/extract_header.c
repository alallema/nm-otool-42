/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extract_header.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 18:28:30 by alallema          #+#    #+#             */
/*   Updated: 2019/02/03 00:48:36 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int			magic_is_valid(t_header *header)
{
	if (header->magic == MH_MAGIC || header->magic == MH_CIGAM
		|| header->magic == MH_MAGIC_64 || header->magic == MH_CIGAM_64)
	{
		return (handle(header));
	}
	else if (header->magic == FAT_MAGIC || header->magic == FAT_MAGIC_64
		|| header->magic == FAT_CIGAM || header->magic == FAT_CIGAM_64)
	{
		return (handle_fat(header));
	}
	else if (ft_strncmp((char *)header->ptr, ARMAG, SARMAG) == 0)
	{
		return (handle_arch(header));
	}
	else
		return (error(ERR_NOT_OBJ, EXIT_FAILURE, header->name, header));
	return (EXIT_SUCCESS);
}

void		extract_magic(t_header *header)
{
	header->magic = *(uint32_t *)header->ptr;
	header->endian = LITTLE;
	if (header->magic == MH_MAGIC || header->magic == MH_CIGAM
		|| header->magic == FAT_MAGIC || header->magic == FAT_CIGAM)
	{
		header->arch = ARCH_32;
	}
	if (header->magic == MH_MAGIC_64 || header->magic == MH_CIGAM_64
		|| header->magic == FAT_MAGIC_64 || header->magic == FAT_CIGAM_64)
	{
		header->arch = ARCH_64;
	}
	if (header->magic == MH_CIGAM || header->magic == MH_CIGAM_64
		|| header->magic == FAT_CIGAM || header->magic == FAT_CIGAM_64)
	{
		header->endian = BIG;
	}
}

int			check_argument(char *arg, t_header *file)
{
	int			fd;
	struct stat	buff;

	if ((fd = open(arg, O_RDONLY)) < 0)
		return (error(ERR_OPEN, EXIT_FAILURE, arg, file));
	if (fstat(fd, &buff) < 0)
		return (error(ERR_READ, EXIT_FAILURE, NULL, file));
	if (buff.st_mode & S_IFDIR)
		return (EXIT_FAILURE);
	file->ptr = mmap(0, buff.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (close(fd))
		return (error(ERR_CLOSED, EXIT_FAILURE, NULL, file));
	if (file->ptr == MAP_FAILED)
		return (error(ERR_MMAP, EXIT_FAILURE, NULL, file));
	file->size = buff.st_size;
	return (EXIT_SUCCESS);
}
