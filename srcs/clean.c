/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 06:52:09 by alallema          #+#    #+#             */
/*   Updated: 2019/04/28 20:33:33 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	clean_file(t_header *file)
{
	ft_memdel((void *)&file->name);
	ft_memdel((void *)&file);
}

int		clean(t_header *file)
{
	if (munmap(file->ptr, file->size) < 0)
		return (error(ERR_MUMAP, EXIT_FAILURE, NULL, NULL));
	ft_memdel((void *)&file->name);
	free_sect_list(file->sect);
	file->sect = NULL;
	return (EXIT_SUCCESS);
}
