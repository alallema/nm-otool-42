/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   section.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 19:31:46 by alallema          #+#    #+#             */
/*   Updated: 2019/04/20 17:23:31 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void		sect_list(t_header *file, char *segname, char *sectname, uint8_t nb)
{
	t_sect		*elem;
	t_sect		*tmp;

	tmp = NULL;
	if (!file || !sectname)
		return ;
	elem = (t_sect *)ft_memalloc(sizeof(t_sect));
	if (!file->sect)
		file->sect = elem;
	else
	{
		tmp = file->sect;
		while (tmp && tmp->next)
			tmp = tmp->next;
		if (tmp)
			((t_sect *)tmp)->next = elem;
	}
	ft_memcpy(elem->segname, segname, 15);
	ft_memcpy(elem->sectname, sectname, 15);
	elem->nsect = nb;
	elem->next = NULL;
}

uint8_t		sect32_list(struct load_command *lc, t_header *file, uint8_t nb)
{
	struct segment_command		*seg;
	struct section				*sect;
	uint8_t						i;

	seg = NULL;
	if (endian(lc->cmd, file) == LC_SEGMENT)
	{
		seg = (struct segment_command *)lc;
		i = nb;
		if (endian(seg->nsects, file))
		{
			sect = (struct section *)(seg + 1);
			while (nb < i + endian(seg->nsects, file))
			{
				sect_list(file, sect->segname, sect->sectname, nb);
				sect++;
				nb++;
			}
		}
	}
	return (nb);
}

uint8_t		sect64_list(struct load_command *lc, t_header *file, uint8_t nb)
{
	struct segment_command_64	*seg_64;
	struct section_64			*sect_64;
	uint8_t						i;

	seg_64 = NULL;
	if (endian(lc->cmd, file) == LC_SEGMENT_64)
	{
		seg_64 = (struct segment_command_64 *)lc;
		i = nb;
		if (endian(seg_64->nsects, file))
		{
			sect_64 = (struct section_64 *)(seg_64 + 1);
			while (nb < i + endian(seg_64->nsects, file))
			{
				sect_list(file, sect_64->segname, sect_64->sectname, nb);
				sect_64++;
				nb++;
			}
		}
	}
	return (nb);
}

void		free_sect_list(t_sect *sect)
{
	t_sect	*tmp;

	while (sect)
	{
		tmp = sect->next;
		ft_memdel((void *)&sect);
		sect = tmp;
	}
	ft_memdel((void *)&sect);
}

char		get_sect_type(t_sect *list, uint8_t nsect)
{
	t_sect	*tmp;
	char	c;

	tmp = list;
	c = '?';
	while (tmp)
	{
		if (tmp->nsect == nsect)
		{
			if (ft_strcmp(tmp->sectname, "__text") == 0)
				c = 't';
			else if (ft_strcmp(tmp->sectname, "__data") == 0)
				c = 'd';
			else if (ft_strcmp(tmp->sectname, "__bss") == 0)
				c = 'b';
			else
				c = 's';
		}
		tmp = ((t_sect *)tmp)->next;
	}
	return (c);
}
