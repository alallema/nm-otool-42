/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 21:58:27 by alallema          #+#    #+#             */
/*   Updated: 2019/02/03 00:48:37 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

char			*call_err(int err)
{
	if (err == ERR_FAT)
		return ("fat file (offset plus size of cputype (18) cpusubtype (0) \
		extends past the end of the file)\n");
	else if (err == ERR_LC_64)
		return ("object (load command 2 fileoff field plus filesize field in \
		LC_SEGMENT_64 extends past the end of the file)\n");
	else if (err == ERR_LC_32)
		return ("object (offset field plus size field of section 0 in \
		LC_SEGMENT command 1 extends past the end of the file)\n");
	else if (err == ERR_LC)
		return ("object (load command 1 extends past the end all load \
		commands in the file)\n");
	else if (err == ERR_OFF)
		return ("archive (offset to next archive member past the end of the\
		archive after member mlx_shaders.o)\n");
	return (NULL);
}

int				error(char *err, int error, char *cmd, void *file)
{
	if (cmd)
	{
		if (((t_header *)file)->exc == NM)
			ft_putstr_fd("nm: ", 2);
		else if (((t_header *)file)->exc == OTOOL)
			ft_putstr_fd("otool: ", 2);
		ft_putstr_fd(cmd, 2);
	}
	if (err == ERR_CORR_64 || err == ERR_CORR_32 || error == ERR_LC_64 ||
		error == ERR_LC_32 || error == ERR_LC || error == ERR_OFF)
		ft_putstr_fd(" truncated or malformed ", 2);
	ft_putstr_fd(err, 2);
	return (error);
}
