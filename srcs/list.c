/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 18:40:47 by alallema          #+#    #+#             */
/*   Updated: 2019/04/28 20:52:50 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void		elem_32(t_nlist *elem)
{
	elem->type = (uint32_t)(elem->type);
	elem->nsect = (uint32_t)(elem->nsect);
	if (elem->value)
		elem->value = (uint32_t)(elem->value);
}

t_nlist		*nlist_set(struct nlist_64 *array, char *strtable, void *file)
{
	t_nlist			*elem;
	char			*str;

	if (!array)
		return (NULL);
	if (array->n_type && N_STAB & endian(array->n_type, file))
		return (NULL);
	str = (strtable + endian(array->n_un.n_strx, file));
	if (str == NULL)
		return (NULL);
	elem = (t_nlist *)ft_memalloc(sizeof(t_nlist));
	if (elem == NULL)
		return (NULL);
	elem->next = NULL;
	return (elem);
}

t_nlist		*nlist_new(struct nlist_64 *array, char *strtable, void *file)
{
	t_nlist			*elem;
	char			*str;

	if ((elem = nlist_set(array, strtable, file)) == NULL)
		return (NULL);
	str = (strtable + endian(array->n_un.n_strx, file));
	if (array && array->n_type && endian(array->n_un.n_strx, file) \
		< ((t_header *)file)->size)
	{
		elem->strx = str;
		elem->type = endian(array->n_type, file);
	}
	else
	{
		elem->strx = ft_strdup("bad string index");
		elem->type = '?';
	}
	elem->type = endian(array->n_type, file);
	elem->nsect = (array->n_sect);
	elem->value = 0;
	if (endian(array->n_value, file))
		elem->value = endian(array->n_value, file);
	if (((t_header *)file)->arch == ARCH_32)
		elem_32(elem);
	return (elem);
}

t_nlist		*nlist_add2(t_nlist *list, t_nlist *elem)
{
	if (!list->next || \
	(ft_strcmp(elem->strx, ((t_nlist *)(list->next))->strx) < 0) || \
	(ft_strcmp(elem->strx, ((t_nlist *)(list->next))->strx) == 0))
	{
		while (list->next && \
			ft_strcmp(elem->strx, ((t_nlist *)(list->next))->strx) == 0 \
			&& elem->value > ((t_nlist *)(list->next))->value)
			list = list->next;
		((t_nlist *)elem)->next = ((t_nlist *)list)->next;
		((t_nlist *)list)->next = elem;
	}
	list = list->next;
	return (list);
}

void		nlist_add(t_nlist **head, t_nlist *elem)
{
	t_nlist	*list;

	list = *head;
	if (!elem)
		return ;
	if (elem && elem->strx && !ft_isprintable(elem->strx))
		elem->strx = ft_strdup("bad string index");
	if (!list || (list && ft_strcmp(elem->strx, list->strx) < 0) || \
			((list && ft_strcmp(elem->strx, list->strx) == 0) &&
			elem->value < list->value))
	{
		((t_nlist *)elem)->next = *head;
		*head = elem;
	}
	else if ((list && ft_strcmp(elem->strx, list->strx) == 0))
	{
		while (list->next && \
			ft_strcmp(elem->strx, ((t_nlist *)(list->next))->strx) == 0 \
			&& elem->value > ((t_nlist *)(list->next))->value)
			list = list->next;
		((t_nlist *)elem)->next = ((t_nlist *)list)->next;
		((t_nlist *)list)->next = elem;
	}
	while (list && ft_strcmp(elem->strx, list->strx) > 0)
		list = nlist_add2(list, elem);
}
