/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sect_otool.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 21:37:29 by alallema          #+#    #+#             */
/*   Updated: 2019/04/20 16:31:05 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	print_name(char *name, t_header *header)
{
	(void)header;
	if (name)
	{
		ft_putstr(name);
		ft_putstr(":\n");
	}
	ft_putstr("Contents of (__TEXT,__text) section");
}

void	print_otool(void *file, uint64_t size, char *data, uint64_t addr)
{
	uint64_t					i;

	i = 0;
	print_name(((t_header *)file)->name, file);
	while (i < size)
	{
		if (i % 16 == 0)
		{
			ft_putchar('\n');
			if (i != 0)
				addr += 16;
			print_value(addr, file, 0);
			ft_putchar('\t');
		}
		char_to_hexa(file, (uint8_t) * data);
		if (((t_header *)file)->endian == LITTLE)
			ft_putchar(' ');
		if (((t_header *)file)->endian == BIG && (i + 1) % 4 == 0)
			ft_putchar(' ');
		data++;
		i++;
	}
	ft_putchar('\n');
}

int		sect64_otool(t_header *file, struct segment_command_64 *seg_64)
{
	struct section_64			*sect_64;
	char						*data;
	uint64_t					addr;

	sect_64 = (struct section_64 *)(seg_64 + 1);
	if (ft_strcmp(sect_64->segname, SEG_TEXT) == 0 &&
		ft_strcmp(sect_64->sectname, SECT_TEXT) == 0)
	{
		if (endian(sect_64->size, file) > file->size)
			return (error(call_err(ERR_LC_64), EXIT_FAILURE, file->name, file));
		data = (file->ptr + endian(sect_64->offset, file));
		addr = endian(sect_64->addr, file);
		print_otool(file, endian(sect_64->size, file), data, addr);
	}
	return (EXIT_SUCCESS);
}

int		sect_otool(t_header *file, struct segment_command *seg)
{
	struct section				*sect;
	char						*data;
	uint64_t					addr;

	sect = (struct section *)(seg + 1);
	if (ft_strcmp(sect->segname, SEG_TEXT) == 0 &&
		ft_strcmp(sect->sectname, SECT_TEXT) == 0)
	{
		if (endian(sect->size, file) > (file->size))
			return (error(call_err(ERR_LC_32), EXIT_FAILURE, file->name, file));
		data = (file->ptr + endian(sect->offset, file));
		addr = endian(sect->addr, file);
		print_otool(file, (uint32_t)endian(sect->size, file), data, addr);
	}
	return (EXIT_SUCCESS);
}

int		lc_otool(void *file, struct load_command *lc)
{
	struct segment_command_64	*seg_64;
	struct segment_command		*seg;

	seg_64 = NULL;
	seg = NULL;
	if (endian(lc->cmd, file) == LC_SEGMENT_64)
	{
		seg_64 = (struct segment_command_64 *)lc;
		if (endian(seg_64->nsects, file))
			return (sect64_otool((t_header *)file, seg_64));
	}
	if (endian(lc->cmd, file) == LC_SEGMENT)
	{
		seg = (struct segment_command *)lc;
		if (endian(seg->nsects, file))
			return (sect_otool((t_header *)file, seg));
	}
	return (EXIT_SUCCESS);
}
