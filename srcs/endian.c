/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   endian.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 20:59:03 by alallema          #+#    #+#             */
/*   Updated: 2019/01/26 19:21:12 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int				endianess(void *file)
{
	return (((t_header *)file)->endian);
}

uint16_t		swap_uint16(uint16_t x, void *file)
{
	if (endianess(file) == BIG)
		return ((x >> 8) | (x << 8));
	return (x);
}

uint32_t		swap_uint32(uint32_t x, void *file)
{
	if (endianess(file) == BIG)
	{
		x = ((x << 8) & 0xFF00FF00) | ((x >> 8) & 0xFF00FF);
		return ((x >> 16) | (x << 16));
	}
	return (x);
}

uint64_t		swap_uint64(uint64_t x, void *file)
{
	if (endianess(file) == BIG)
	{
		x = ((x << 8) & 0xFF00FF00FF00FF00ULL) |
			((x >> 8) & 0x00FF00FF00FF00FFULL);
		x = ((x << 16) & 0xFFFF0000FFFF0000ULL) |
			((x >> 16) & 0x0000FFFF0000FFFFULL);
		return (x << 32) | (x >> 32);
	}
	return (x);
}

uint64_t		endian(uint64_t x, void *file)
{
	t_header	*ptr;

	ptr = file;
	if (ptr->endian == BIG)
	{
		if (ptr->arch == ARCH_16)
			return ((x >> 8) | (x << 8));
		if (ptr->arch == ARCH_32)
		{
			x = ((x << 8) & 0xFF00FF00) | ((x >> 8) & 0xFF00FF);
			return ((uint32_t)((x >> 16) | (x << 16)));
		}
		if (ptr->arch == ARCH_64)
		{
			x = ((x << 8) & 0xFF00FF00FF00FF00ULL) |
				((x >> 8) & 0x00FF00FF00FF00FFULL);
			x = ((x << 16) & 0xFFFF0000FFFF0000ULL) |
				((x >> 16) & 0x0000FFFF0000FFFFULL);
			return (x << 32) | (x >> 32);
		}
	}
	return (x);
}
