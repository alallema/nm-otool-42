/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fat_tools.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 21:36:03 by alallema          #+#    #+#             */
/*   Updated: 2019/04/28 20:49:52 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

uint32_t	power2(uint32_t nb)
{
	uint32_t	power;

	if (nb == 0)
		return (0);
	power = 1;
	while (nb > 0)
	{
		power *= 2;
		nb--;
	}
	return (power);
}

uint32_t	align(uint32_t size, uint32_t padd)
{
	padd = power2(padd);
	return (((size) + (padd - 1)) & ~(padd - 1));
}

int			arch64_exist(t_header *fat, t_header *file)
{
	(void)file;
	fat->endian = LITTLE;
	fat->arch = ARCH_64;
	fat->name = file->name;
	if (ft_strncmp((char *)fat->ptr, ARMAG, SARMAG) == 0)
		return (handle_arch(fat));
	return (handle(fat));
}

int			fat_header_set(t_header *fat, t_header *file, uint32_t nfat,\
			struct fat_arch *arch)
{
	int ret;

	ret = 0;
	if (file->exc == OTOOL)
		fat->exc = OTOOL;
	if (file->exc == NM)
		fat->exc = NM;
	if ((ret = check_arch64_exist(nfat, arch, fat, file)) >= 0)
		return (ret);
	fat->ptr = (void *)file->ptr + (align(sizeof(struct fat_header) +\
	nfat * sizeof(*arch), endian(arch->align, file)));
	return (-1);
}

int			check_arch64_exist(uint32_t nfat, struct fat_arch *arch,\
			t_header *fat, t_header *file)
{
	uint32_t	i;
	uint32_t	size;
	size_t		max_size;

	i = 0;
	max_size = sizeof(*arch);
	fat->ptr = (void *)file->ptr + (align(sizeof(struct fat_header *) +\
				nfat * sizeof(*arch), endian(arch->align, file)));
	while (i < nfat)
	{
		size = align(endian(arch->size, file), endian(arch->align, file));
		fat->size = size;
		max_size += size;
		if (max_size > file->size)
			return (error(call_err(ERR_FAT), EXIT_FAILURE, file->name, file));
		if (endian(arch->cputype, file) == (CPU_TYPE_X86 | CPU_ARCH_ABI64))
			return (arch64_exist(fat, file));
		fat->ptr = (void *)(file->ptr + size + endian(arch->offset, file));
		i++;
		arch++;
	}
	return (-1);
}
