/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 23:15:43 by alallema          #+#    #+#             */
/*   Updated: 2019/02/03 00:49:24 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

uint32_t	nb_command(void *file)
{
	struct mach_header		*header32;
	struct mach_header_64	*header64;

	header32 = ((t_header *)file)->ptr;
	header64 = ((t_header *)file)->ptr;
	if (((t_header *)file)->arch == ARCH_32)
		return (endian(header32->ncmds, file));
	if (((t_header *)file)->arch == ARCH_64)
		return (endian(header64->ncmds, file));
	return (EXIT_FAILURE);
}

void		*arch_load_command(void *file)
{
	if (((t_header *)file)->arch == ARCH_32)
		return (((t_header *)file)->ptr + sizeof(struct mach_header));
	if (((t_header *)file)->arch == ARCH_64)
		return (((t_header *)file)->ptr + sizeof(struct mach_header_64));
	return (NULL);
}

int			check_cmd_size(struct load_command *lc, void *file, uint32_t ncmds)
{
	(void)ncmds;
	if (((t_header *)file)->arch == ARCH_64)
	{
		if (endian(lc->cmdsize, file) % 8 != 0)
			return (error(ERR_CORR_64, EXIT_FAILURE, ((t_header *)file)->name,\
			file));
	}
	else if (((t_header *)file)->arch == ARCH_32)
	{
		if (endian(lc->cmdsize, file) % 4 != 0)
			return (error(ERR_CORR_32, EXIT_FAILURE, ((t_header *)file)->name,\
			file));
	}
	return (EXIT_SUCCESS);
}

int			lc_nm(void *file, struct load_command *lc, uint8_t nb)
{
	struct symtab_command	*sym;

	if (endian(lc->cmd, file) == LC_SYMTAB)
	{
		sym = (struct symtab_command *)lc;
		if (endian(sym->stroff, file) + endian(lc->cmdsize, file) \
			> ((t_header*)file)->size)
			return (error(call_err(ERR_LC), 1, ((t_header *)file)->name, file));
		return (symbol_list(endian(sym->nsyms, file), endian(sym->symoff, file),
				endian(sym->stroff, file), file));
	}
	else
	{
		if (((t_header *)file)->arch == ARCH_64)
			return ((uint8_t)(sect64_list(lc, file, nb)));
		else if (((t_header *)file)->arch == ARCH_32)
			return ((uint8_t)(sect32_list(lc, file, nb)));
	}
	return (0);
}

int			handle(void *file)
{
	uint32_t				ncmds[2];
	struct load_command		*lc;
	uint8_t					nb;

	ncmds[1] = 0;
	nb = 1;
	((t_header *)file)->sect = NULL;
	ncmds[0] = nb_command(file);
	lc = (struct load_command *)arch_load_command(file);
	while (lc && ncmds[1]++ < ncmds[0])
	{
		if (check_cmd_size(lc, file, ncmds[0]))
			return (EXIT_FAILURE);
		if (((t_header*)file)->exc == NM && endian(lc->cmd, file) != LC_SYMTAB)
			nb = lc_nm(file, lc, nb);
		if (((t_header*)file)->exc == NM && endian(lc->cmd, file) == LC_SYMTAB)
			return (lc_nm(file, lc, nb));
		if (((t_header*)file)->exc == OTOOL)
			if (lc_otool(file, lc) != EXIT_SUCCESS)
				return (EXIT_FAILURE);
		if (endian(lc->cmdsize, file) > ((t_header*)file)->size)
			return (error(call_err(ERR_LC), 1, ((t_header *)file)->name, file));
		lc = (void *)lc + endian(lc->cmdsize, file);
	}
	return (EXIT_SUCCESS);
}
