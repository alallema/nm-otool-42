/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 18:16:41 by alallema          #+#    #+#             */
/*   Updated: 2019/04/20 17:39:41 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

t_header	*set_header_file(void)
{
	t_header	*file;

	file = (t_header *)ft_memalloc(sizeof(t_header));
	if (file == NULL)
		return (NULL);
	file->sect = NULL;
	file->exc = OTOOL;
	return (file);
}

void		launch_otool(t_header *file, char *av)
{
	file->name = ft_strdup(av);
	extract_magic(file);
}

int			main(int ac, char **av)
{
	t_header	*file;
	int			i;

	i = 1;
	if (ac < 2)
		return (error(ERR_NUM_ARG, EXIT_FAILURE, NULL, NULL));
	if (!(file = set_header_file()))
		return (error(ERR_MALLOC, EXIT_FAILURE, NULL, NULL));
	while (i < ac)
	{
		if (check_argument(av[i], file) == EXIT_SUCCESS)
		{
			launch_otool(file, av[i]);
			if (magic_is_valid(file) == EXIT_FAILURE && ac < 3)
			{
				ft_memdel((void *)&file);
				return (EXIT_FAILURE);
			}
			else
				clean(file);
		}
		i++;
	}
	ft_memdel((void *)&file);
	return (EXIT_SUCCESS);
}
