/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 21:34:22 by alallema          #+#    #+#             */
/*   Updated: 2019/04/28 20:54:51 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void		print_cputype(char *name, char *arch, void *file)
{
	if (((t_header *)file)->exc == NM)
		ft_putstr("\n");
	ft_putstr(name);
	if (((t_header *)file)->exc == NM)
		ft_putstr(" (for ");
	if (((t_header *)file)->exc == OTOOL)
		ft_putstr(" (");
	ft_putstr(arch);
}

void		dispatch_print_cputype(struct fat_arch *arch, t_header *file)
{
	if (endian(arch->cputype, file) == CPU_TYPE_POWERPC)
		print_cputype(file->name, "architecture ppc):\n", file);
	else if (endian(arch->cputype, file) == CPU_TYPE_POWERPC64)
		print_cputype(file->name, "architecture ppc64):\n", file);
	else if (endian(arch->cputype, file) == (CPU_TYPE_X86 | CPU_TYPE_I386))
		print_cputype(file->name, "architecture i386):\n", file);
	else if (endian(arch->cputype, file) == CPU_TYPE_ARM)
	{
		if (endian(arch->cpusubtype, file) == CPU_SUBTYPE_ARM_V7)
			print_cputype(file->name, "architecture armv7):\n", file);
		else if (endian(arch->cpusubtype, file) == CPU_SUBTYPE_ARM_V7S)
			print_cputype(file->name, "architecture armv7s):\n", file);
		else
			print_cputype(file->name, "architecture armv):\n", file);
	}
	else if (endian(arch->cputype, file) == CPU_TYPE_ARM64)
		print_cputype(file->name, "architecture arm64):\n", file);
}

void		set_cputype(int cputype, t_header *fat)
{
	if (cputype == CPU_TYPE_POWERPC || cputype == CPU_TYPE_POWERPC)
		fat->endian = BIG;
	else if (cputype == (CPU_TYPE_X86 | CPU_TYPE_I386) ||\
		cputype == CPU_TYPE_ARM || cputype == CPU_TYPE_ARM64)
		fat->endian = LITTLE;
	if (cputype == CPU_TYPE_POWERPC || cputype == CPU_TYPE_ARM ||\
		cputype == (CPU_TYPE_X86 | CPU_TYPE_I386))
		fat->arch = ARCH_32;
	else if (cputype == CPU_TYPE_POWERPC64 || cputype == CPU_TYPE_ARM64)
		fat->arch = ARCH_64;
}

void		handle_fat2(t_header *file, t_header *fat, struct fat_arch *arch)
{
	dispatch_print_cputype(arch, file);
	set_cputype(endian(arch->cputype, file), fat);
	fat->size = align(endian(arch->size, file), endian(arch->align, file));
}

int			handle_fat(t_header *file)
{
	uint32_t				nfat;
	uint32_t				i;
	struct fat_arch			*arch;
	t_header				fat;
	int						ret;

	nfat = endian(((struct fat_header *)file->ptr)->nfat_arch, file);
	i = 0;
	ret = 0;
	arch = (void *)(file->ptr + sizeof(struct fat_header));
	fat.name = NULL;
	if ((ret = fat_header_set(&fat, file, nfat, arch)) >= 0)
		return (ret);
	while (i < nfat)
	{
		handle_fat2(file, &fat, arch);
		if (fat.size == 0)
			return (EXIT_FAILURE);
		handle(&fat);
		free_sect_list(fat.sect);
		fat.ptr = (void *)(file->ptr + fat.size + endian(arch->offset, file));
		i++;
		arch++;
	}
	return (EXIT_SUCCESS);
}
