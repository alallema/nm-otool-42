/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symbol_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 07:13:08 by alallema          #+#    #+#             */
/*   Updated: 2019/04/28 20:40:28 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int		is_in(char *str, char c)
{
	int		i;

	i = 0;
	while (i)
	{
		if (str[i] == c)
			return (1);
		i++;
	}
	return (0);
}

void	free_symbol_list(t_nlist *list)
{
	t_nlist			*tmp;

	while (list)
	{
		tmp = list->next;
		if (list && list->strx && \
			ft_strcmp(list->strx, "bad string index") == 0)
			ft_memdel((void *)&list->strx);
		ft_memdel((void *)&list);
		list = tmp;
	}
	ft_memdel((void *)&list);
}

int		error_lc(struct nlist *a32, struct nlist_64 *a64, int i, t_header *file)
{
	if (file->arch == ARCH_64)
	{
		if ((unsigned long)(&a64[i]) - (unsigned long)file->ptr > file->size)
			return (error(call_err(ERR_LC_64), EXIT_FAILURE, file->name, file));
	}
	else if (((t_header *)file)->arch == ARCH_32)
	{
		if ((unsigned long)(&a32[i]) - (unsigned long)file->ptr > file->size)
			return (error(call_err(ERR_LC_32), EXIT_FAILURE, file->name, file));
	}
	return (EXIT_SUCCESS);
}

int		symbol_list(int nsyms, int symoff, int stroff, t_header *file)
{
	int				i;
	char			*strtable;
	struct nlist_64	*array64;
	struct nlist	*array32;
	t_nlist			*list;

	i = 0;
	list = NULL;
	array32 = (void *)(file->ptr + symoff);
	array64 = (void *)(file->ptr + symoff);
	strtable = (void *)(file->ptr + stroff);
	while (i < nsyms)
	{
		if (error_lc(array32, array64, i, file) == EXIT_FAILURE)
			return (EXIT_FAILURE);
		if (file->arch == ARCH_64)
			nlist_add(&list, nlist_new(&array64[i], strtable, file));
		else if (((t_header *)file)->arch == ARCH_32)
			nlist_add(&list, nlist_new((struct nlist_64 *)&array32[i],\
				strtable, file));
		i++;
	}
	print_nlist(list, file);
	free_symbol_list(list);
	return (EXIT_SUCCESS);
}
