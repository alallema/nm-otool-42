/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_arch.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 19:53:55 by alallema          #+#    #+#             */
/*   Updated: 2019/02/02 22:53:13 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	print_arch(t_header *file)
{
	if (file->exc == OTOOL)
	{
		ft_putstr("Archive : ");
		ft_putstr(file->name);
		ft_putstr("\n");
	}
}

void	print_arch_file(t_header *file, struct ar_hdr *header)
{
	if (file->exc == NM)
		ft_putstr("\n");
	ft_putstr(file->name);
	ft_putstr("(");
	ft_putstr(header->ar_fmag + 2);
	ft_putstr("):\n");
}

int		launch_handle(t_header *file)
{
	char	*name;

	name = file->name;
	file->name = NULL;
	extract_magic(file);
	if (magic_is_valid(file) == EXIT_FAILURE)
		return (EXIT_FAILURE);
	if (file->sect)
		free_sect_list(file->sect);
	file->sect = NULL;
	file->name = name;
	return (EXIT_SUCCESS);
}

int		handle_arch(t_header *file)
{
	struct ar_hdr			*header;
	uint32_t				size;
	void					*ptr;
	int						length;

	header = file->ptr + SARMAG;
	length = 0;
	size = ft_atoi((header->ar_size)) + sizeof(*header) + SARMAG;
	ptr = file->ptr;
	print_arch(file);
	while (size < file->size)
	{
		header = ptr + size;
		print_arch_file(file, header);
		size = ft_atoi((header->ar_size)) + sizeof(*header) + size;
		if (size > file->size || ft_atoi(header->ar_size) <= 1)
			return (error(call_err(ERR_OFF), EXIT_FAILURE, file->name, file));
		length = ft_atoi(header->ar_name + 3);
		file->ptr = (void *)header + sizeof(*header) + length;
		if (launch_handle(file) == EXIT_FAILURE)
			return (EXIT_FAILURE);
	}
	file->ptr = ptr;
	return (EXIT_SUCCESS);
}
