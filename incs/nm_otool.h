/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm-otool.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 20:08:41 by alallema          #+#    #+#             */
/*   Updated: 2019/04/28 20:46:56 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_OTOOL_H
# define NM_OTOOL_H

# include "archive.h"
# include "libft.h"

/*
** System errors
*/

# define ERR_MALLOC		": Bad allocation\n"
# define ERR_MMAP		": Error mmap\n"
# define ERR_MUMAP		": Error munmap\n"
# define ERR_READ		": Input error\n"
# define ERR_OPEN		": Error open file\n"
# define ERR_CLOSED		": Error close file\n"

# define ERR_NUM_ARG	": argument required\n"

# define ERR_INDEX		"Bad string index\n"
# define ERR_NOT_OBJ	" The file was not recognized as a valid object file\n"
# define ERR_CORR_64	"object (load command 0 cmdsize not a multiple of 8)\n"
# define ERR_CORR_32	"object (load command 0 cmdsize not a multiple of 4)\n"
# define ERR_FAT		0
# define ERR_LC_64		1
# define ERR_LC_32		2
# define ERR_LC			3
# define ERR_OFF		4

int			clean(t_header *file);
void		clean_file(t_header *file);
int			check_argument(char *arg, t_header *file);

void		extract_magic(t_header *file);
int			magic_is_valid(t_header *header);
int			handle(void *ptr);
int			handle_fat(t_header *ptr);
int			handle_arch(t_header *file);
int			symbol_list(int nsyms, int symoff, int stroff, t_header *file);
void		symbol_list64(int nsyms, int symoff, int stroff, void *file);
void		symbol_list32(int nsyms, int symoff, int stroff, void *file);
int			arch64_exist(t_header *fat, t_header *file);
int			check_arch64_exist(uint32_t nfat, struct fat_arch *arch,\
			t_header *fat, t_header *file);
int			fat_header_set(t_header *fat, t_header *file, uint32_t nfat,\
			struct fat_arch *arch);

int			endianess(void *file);
uint64_t	endian(uint64_t x, void *file);
uint32_t	swap_uint32(uint32_t x, void *file);
uint64_t	swap_uint64(uint64_t x, void *file);
uint32_t	power2(uint32_t nb);
uint32_t	align(uint32_t size, uint32_t padd);

void		putnbr_base(unsigned long n, int base);
void		print_arg(char *arg);
void		print_value(uint64_t value, void *file, char c);
int			char_to_hexa(void *file, uint8_t c);

t_nlist		*nlist_new(struct nlist_64 *array, char *strtable, void *file);
t_nlist		*nlist_new32(struct nlist *array, char *strtable, void *file);
void		nlist_add(t_nlist **list, t_nlist *elem);
int			nlist_sort(t_nlist **list);
void		sect_list(t_header *file, char *segname, char*sectname, uint8_t nb);
char		get_sect_type(t_sect *list, uint8_t nsect);
void		print_nlist(void *list, void *file);

void		clean_list(t_nlist **list);
void		free_sect_list(t_sect *sect);

uint8_t		sect32_list(struct load_command *lc, t_header *file, uint8_t nb);
uint8_t		sect64_list(struct load_command *lc, t_header *file, uint8_t nb);

int			lc_otool(void *file, struct load_command *lc);

#endif
