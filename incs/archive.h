/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   archive.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <alallema@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 18:13:49 by alallema          #+#    #+#             */
/*   Updated: 2019/01/29 17:18:32 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARCHIVE_H
# define ARCHIVE_H

# include <sys/mman.h>
# include <mach-o/loader.h>
# include <mach-o/fat.h>
# include <ar.h>
# include <mach-o/nlist.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <stdlib.h>
# include "libft.h"

# define LITTLE 1
# define BIG 0

# define OTOOL 1
# define NM 0

# define ERR_ELEM 3

# define ARCH_32 0
# define ARCH_64 1
# define ARCH_16 2

# define NONE 0
# define MAX_VALUE 0xFFFFFFFFFFFFFFFF

int				error(char *err, int error, char *cmd, void *file);
char			*call_err(int err);

typedef struct	s_sect
{
	void		*next;
	uint8_t		nsect;
	char		segname[16];
	char		sectname[16];
}				t_sect;

typedef struct	s_header
{
	void		*ptr;
	uint32_t	magic;
	int			endian;
	int			arch;
	int			exc;
	size_t		size;
	char		*name;
	t_sect		*sect;
}				t_header;

typedef struct	s_nlist
{
	void		*next;
	char		*strx;
	int			type;
	uint8_t		nsect;
	uint64_t	value;
}				t_nlist;

#endif
