RED =			\033[31m
GRE =			\033[32m
YEL =			\033[33m
BLU =			\033[34m
PUR =			\033[94m
PIN =			\033[1;35m
PRR =			\033[0;36m
STD =			\033[39m

NAME_NM =		ft_nm
NAME_OTOOL =	ft_otool

IDIR =			./incs/
ILIB =			./libft/includes
INCS =			archive.h		\
				nm_otool.h

INCC =			$(addprefix $(IDIR),$(INCS))

LDIR =			./libft
LIBS =			-lft

VPATH =			./srcs/:

SRCS_NM =		nm.c				\
				handle.c			\
				handle_fat.c		\
				handle_arch.c		\
				extract_header.c	\
				endian.c			\
				section.c			\
				print.c				\
				symbol_list.c		\
				list.c				\
				clean.c				\
				sect_otool.c		\
				fat_tools.c			\
				error.c

SRCS_OTOOL =	otool.c				\
				extract_header.c	\
				handle.c			\
				handle_fat.c		\
				handle_arch.c		\
				print.c				\
				symbol_list.c		\
				section.c			\
				endian.c			\
				list.c				\
				clean.c				\
				sect_otool.c		\
				fat_tools.c			\
				error.c

SRCC =		$(addprefix $(SRCS_NM),$(SRCS_OTOOL))

ODIR =			./objs/

OBJS_NM =		$(SRCS_NM:.c=.o)
OBCC_NM =		$(addprefix $(ODIR),$(OBJS_NM))

OBJS_OTOOL =	$(SRCS_OTOOL:.c=.o)
OBCC_OTOOL =	$(addprefix $(ODIR),$(OBJS_OTOOL))

NORM =			$(SRCC) $(INCC)

CFLAG =			-MD -Wall -Wextra -Werror -I$(IDIR)

all: $(NAME_NM) $(NAME_OTOOL)

nm: $(NAME_NM)

otool: $(NAME_OTOOL)

$(NAME_NM): header $(OBCC_NM)
	@echo "  ${PUR}++ Compilation ++ :${STD} $@"
	@make -C ./libft/
	@gcc -g $(CFLAG) $(OBCC_NM) -L$(LDIR) $(LIBS) -o $(NAME_NM)
	@echo "  ${PIN}Compilation terminee !${STD}"

$(NAME_OTOOL): header $(OBCC_OTOOL)
	@echo "  ${PUR}++ Compilation ++ :${STD} $@"
	@make -C ./libft/
	@gcc -g $(FLAG) $(OBCC_OTOOL) -L$(LDIR) $(LIBS) -o $(NAME_OTOOL)
	@echo "  ${PIN}Compilation terminee !${STD}"

$(ODIR)%.o: $(SRCC_PATH)%.c
	@echo "  ${PUR}+Compilation :${STD} $^"
	@mkdir -p $(ODIR)
	@gcc $^ $(FLAG) -c -o $@ -I$(IDIR) -I$(ILIB)

header:
	@echo "${PRR}"
	@echo "  ===================="
	@echo "  |  Projet Nm-Otool  |"
	@echo "  ===================="
	@echo "${STD}"

norme: header
	@echo "${PRR}  Verification de la norme${STD}\n"
	@norminette $(NORM)
	@echo "${RED}  \nTotal errors :${STD}" $(shell norminette $(NORM) | grep -v "Norme" | wc -l)

clean: header
	@echo "  ${RED}-Delete all object files${STD}"
	@rm -rf $(ODIR)
	@make -C ./libft/ fclean
	@rm -f $(OBCC)

fclean: clean
	@rm -f $(NAME_NM) $(NAME_OTOOL)
	@make -C ./libft/ fclean
	@echo "  ${RED}-Delete objects and binary${STD}"

re: fclean all
