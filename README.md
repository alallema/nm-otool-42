
# nm-otool

## nm
nm lists the symbols from object files objfile….
If no object files are listed as arguments, nm assumes the file a.out.

### magic number
```c
#define MH_MAGIC     0xfeedface    /* the mach magic number */
#define MH_CIGAM     0xcefaedfe    /* NXSwapInt(MH_MAGIC) */
#define MH_MAGIC_64  0xfeedfacf    /* the 64-bit mach magic number */
#define MH_CIGAM_64  0xcffaedfe    /* NXSwapInt(MH_MAGIC_64) */
#define FAT_MAGIC    0xcafebabe    /* the fat magic number */
#define FAT_CIGAM    0xbebafeca    /* NXSwapLong(FAT_MAGIC) */
#define FAT_MAGIC_64 0xcafebabf    /* the 64-bit fat magic number */
#define FAT_CIGAM_64 0xbfbafeca    /* NXSwapLong(FAT_MAGIC_64) */
```

### header
```c
struct mach_header {
	uint32_t        magic;          /* mach magic number identifier */
	cpu_type_t      cputype;        /* cpu specifier */
	cpu_subtype_t   cpusubtype;     /* machine specifier */
	uint32_t        filetype;       /* type of file */
	uint32_t        ncmds;          /* number of load commands */
	uint32_t        sizeofcmds;     /* the size of all the load commands */
	uint32_t        flags;          /* flags */
};
```
```c
struct fat_header {
	uint32_t        magic;          /* FAT_MAGIC or FAT_MAGIC_64 */
	uint32_t        nfat_arch;      /* number of structs that follow */
};
```

### fat arch
```c
struct fat_arch { /* for 32-64 architectures */
	cpu_type_t      cputype;        /* cpu specifier (int) */
	cpu_subtype_t   cpusubtype;     /* machine specifier (int) */
	uint32_t        offset;         /* file offset to this object file */
	uint32_t        size;           /* size of this object file */
	uint64_t        offset;         /* -64- file offset to this object file */
	uint64_t        size;           /* -64- size of this object file */
	uint32_t        align;          /* alignment as a power of 2 */
	uint32_t        reserved;       /* -64- reserved */
};
```

### load command
```c
struct load_command {
	uint32_t cmd;           /* type of load command */
	uint32_t cmdsize;       /* total size of command in bytes */
};
```

load_command + offset cmdsize -> next load command
32bit -> multiple 4
64bit -> multiple 8

mach-o -> header
load-command
segment 1 ++
    -> section beetween 0 and 255


### segment
```c
struct segment_command { /* for 32-64bit architectures */
	uint32_t        cmd;            /* LC_SEGMENT */
	uint32_t        cmdsize;        /* includes sizeof section structs */
	char            segname[16];    /* segment name */
	uint32_t        vmaddr;         /* -32- memory address of this segment */
	uint32_t        vmsize;         /* -32- memory size of this segment */
	uint32_t        fileoff;        /* -32- file offset of this segment */
	uint32_t        filesize;       /* -32- amount to map from the file */
	uint64_t        vmaddr;         /* -64- memory address of this segment */
	uint64_t        vmsize;         /* -64- memory size of this segment */
	uint64_t        fileoff;        /* -64- file offset of this segment */
	uint64_t        filesize;       /* -64- amount to map from the file */
	vm_prot_t       maxprot;        /* maximum VM protection */
	vm_prot_t       initprot;       /* initial VM protection */
	uint32_t        nsects;         /* number of sections in segment */
	uint32_t        flags;          /* flags */
};
```

### section
```c
struct section { /* for 32-64bit architectures */
	char            sectname[16];   /* name of this section */
	char            segname[16];    /* segment this section goes in */
	uint32_t        addr;           /* -32- memory address of this section */
	uint32_t        size;           /* -32- size in bytes of this section */
	uint64_t        addr;           /* -64- memory address of this section */
	uint64_t        size;           /* -64- size in bytes of this section */
	uint32_t        offset;         /* file offset of this section */
	uint32_t        align;          /* section alignment (power of 2) */
	uint32_t        reloff;         /* file offset of relocation entries */
	uint32_t        nreloc;         /* number of relocation entries */
	uint32_t        flags;          /* flags (section type and attributes)*/
	uint32_t        reserved1;      /* reserved (for offset or index) */
	uint32_t        reserved2;      /* reserved (for count or sizeof) */
	uint32_t        reserved3;      /* -64- reserved */
};
```

### symtab
```c
struct symtab_command {
	uint32_t        cmd;            /* LC_SYMTAB */
	uint32_t        cmdsize;        /* sizeof(struct symtab_command) */
	uint32_t        symoff;         /* symbol table offset */
	uint32_t        nsyms;          /* number of symbol table entries */
	uint32_t        stroff;         /* string table offset */
	uint32_t        strsize;        /* string table size in bytes */
};
```

### nlist
```c
struct nlist {
	union {
		uint32_t n_strx;        /* index into the string table */
	} n_un;
	uint8_t n_type;         /* type flag, see below */
	uint8_t n_sect;         /* section number or NO_SECT */
	int16_t n_desc;         /* see <mach-o/stab.h> */
	uint32_t n_value;       /* -32- value of this symbol (or stab offset) */
	uint64_t n_value;       /* -64- value of this symbol (or stab offset) */
};
```

### type
```c
/*
 * The n_type field really contains four fields:
 *      unsigned char N_STAB:3,
 *                    N_PEXT:1,
 *                    N_TYPE:3,
 *                    N_EXT:1;
 * which are used via the following masks.
 */
#define N_STAB  0xe0  /* if any of these bits set, a symbolic debugging entry */
#define N_PEXT  0x10  /* private external symbol bit */
#define N_TYPE  0x0e  /* mask for the type bits */
#define N_EXT   0x01  /* external symbol bit, set for external symbols */

/*
 * Only symbolic debugging entries have some of the N_STAB bits set and if any
 * of these bits are set then it is a symbolic debugging entry (a stab).  In
 * which case then the values of the n_type field (the entire field) are given
 * in <mach-o/stab.h>
 */

/*
 * Values for N_TYPE bits of the n_type field.
 */
#define N_UNDF  0x0             /* undefined, n_sect == NO_SECT */
#define N_ABS   0x2             /* absolute, n_sect == NO_SECT */
#define N_SECT  0xe             /* defined in section number n_sect */
#define N_PBUD  0xc             /* prebound undefined (defined in a dylib) */
#define N_INDR  0xa             /* indirect */
```

unsigned int NXSwapInt(unsigned int inv);


```c
/*
 *      Machine types known by all.
 */

#define CPU_TYPE_ANY            ((cpu_type_t) -1)

#define CPU_TYPE_VAX            ((cpu_type_t) 1)
/* skip                         ((cpu_type_t) 2)        */
/* skip                         ((cpu_type_t) 3)        */
/* skip                         ((cpu_type_t) 4)        */
/* skip                         ((cpu_type_t) 5)        */
#define CPU_TYPE_MC680x0        ((cpu_type_t) 6)
#define CPU_TYPE_X86            ((cpu_type_t) 7)
#define CPU_TYPE_I386           CPU_TYPE_X86            /* compatibility */
#define CPU_TYPE_X86_64         (CPU_TYPE_X86 | CPU_ARCH_ABI64)

/* skip CPU_TYPE_MIPS           ((cpu_type_t) 8)        */
/* skip                         ((cpu_type_t) 9)        */
#define CPU_TYPE_MC98000        ((cpu_type_t) 10)
#define CPU_TYPE_HPPA           ((cpu_type_t) 11)
#define CPU_TYPE_ARM            ((cpu_type_t) 12)
#define CPU_TYPE_ARM64          (CPU_TYPE_ARM | CPU_ARCH_ABI64)
#define CPU_TYPE_MC88000        ((cpu_type_t) 13)
#define CPU_TYPE_SPARC          ((cpu_type_t) 14)
#define CPU_TYPE_I860           ((cpu_type_t) 15)
/* skip CPU_TYPE_ALPHA          ((cpu_type_t) 16)       */
/* skip                         ((cpu_type_t) 17)       */
#define CPU_TYPE_POWERPC                ((cpu_type_t) 18)
#define CPU_TYPE_POWERPC64              (CPU_TYPE_POWERPC | CPU_ARCH_ABI64)
```
```c
struct ar_hdr {
	char ar_name[16];               /* name */
	char ar_date[12];               /* modification time */
	char ar_uid[6];                 /* user id */
	char ar_gid[6];                 /* group id */
	char ar_mode[8];                /* octal file permissions */
	char ar_size[10];               /* size in bytes */
#define ARFMAG  "`\n"
	char ar_fmag[2];                /* consistency check */
};
```
